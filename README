This is the GeoGateway project.  To install, you need to install the NPM package manager, Node.js, Git, and MongoDB. MongoDB's default data directory is /data/db, so either make this directory and give it the correct permissions or else change the data directory.  You may already have Git installed. Just type "git" at the command line to see.

INSTALLATION
Run the following commands at the shell prompt:

* cd $INSTALL_PATH
   - You don't need this environment variable set, but we will use it for clarity.

* git clone https://marpierc_iu@bitbucket.org/marpierc_iu/geogateway.git

* cd $INSTALL_PATH/geogateway

* npm install
   - This will install the packages listed in packages.json.
   - Will need to put npm in your PATH
   - ex: export PATH=~/node-v0.10.36-linux-x64/bin/:$PATH

* cd $INSTALL_PATH/geogateway/html

* bower install  

* mkdir -p $INSTALL_PATH/geogateway/html/userUploads  (make sure this directory exists)

RUNNING THE SERVICE
Run the following commands

* mongod
   - This starts MongoDB and can be run from anywhere.
   - You need mongod in your path
   - ex: export PATH=$HOME/mongodb-linux-x86_64-2.6.7/bin/:$PATH

* cd $INSTALL_PATH/geogateway

* cp sample-config.js config.js
   - Edit this file for your local installation.

* node GeoGatewayServer.js

USING THE INTERFACE
Point your browser to http://localhost:3000/main.html

UPDATING THIRD PARTY DEPENDENCIES
To keep libraries up to date, periodically run the following:

* npm update (from $INSTALL_PATH/geogateway)

* bower update (from $INSTALL_PATH/geogateway/html)


## **Optioanl note on python installation** ##    
This is needed to run external application scripts.

Download and Install https://store.continuum.io/cshop/anaconda/  
verify the installation by /usr/bin/env python
 
then in python, find out matplotlib configuration file:  
>>> import matplotlib
>>> matplotlib.matplotlib_fname()
change the backend: 
backend      : Qt4Agg  => backend      : Agg

## Enable JSONP callback on GeoServer ##  
modify startup.sh  
```
#!bash
if [ -z "$JAVA_OPTS" ]; then
export JAVA_OPTS="-XX:MaxPermSize=128m -DENABLE_JSONP=true"
```